//
//  HeaderView.swift
//  bnetLayout02
//
//  Created by Denis Velikanov on 23.09.2021.
//

import Foundation
import UIKit


class HeaderView: UIView {
    
    let titleView = UILabel()
    var titleText: String = ""
    
    func setupUI() {
        self.addSubview(titleView)
        titleView.text = titleText
        titleView.textColor = .white
        titleView.font = UIFont.boldSystemFont(ofSize: 22)
        backgroundColor = UIColor(red: 0.082, green: 0.129, blue: 0.192, alpha: 1)
        
    }
    
    func constraintUI() {
        titleView.anchor(top: self.safeAreaLayoutGuide.topAnchor,
                         leading: self.safeAreaLayoutGuide.leadingAnchor,
                         bottom: self.safeAreaLayoutGuide.bottomAnchor,
                         trailing: self.safeAreaLayoutGuide.trailingAnchor,
                         padding: .init(top: 5, left: 0, bottom: 10, right: 10))
    }
    
    init(titleText: String) {
        super.init(frame: .zero)
        self.titleText = titleText
        setupUI()
        constraintUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
}
