//
//  ViewController.swift
//  bnetLayout02
//
//  Created by Denis Velikanov on 23.09.2021.
//
import UIKit


enum CellTypes: Int, CaseIterable {
    case popular,
         continueWatching,
         newLectures,
         courses,
         newPodcasts
}

class ViewController: UIViewController {
    
    let highStack = UIView()
    let companyLabel = UIImageView()
    let searchView = UIImageView()
    
    let lowScreenMenu = UIView()
    let lowStack = UIStackView()
    
    let browseButton = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
    let lecturesButton = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
    let coursesButton = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
    let profileButton = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
    
    let tableView = UITableView()
    
    let sectionType : [CellTypes] = [.popular, .continueWatching, .newLectures, .courses, .newPodcasts]
        
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        constraintUI()
        view.layer.backgroundColor = UIColor(red: 0.082, green: 0.129, blue: 0.192, alpha: 1).cgColor
        tableView.backgroundColor =  UIColor(red: 0.082, green: 0.129, blue: 0.192, alpha: 1)
        registerCells()
        
    }
    
    private func registerCells() {
        tableView.register(PopularLecturesTableCell.self, forCellReuseIdentifier: PopularLecturesTableCell.identifier)
        tableView.register(ContinueWatchingTableCell.self, forCellReuseIdentifier: ContinueWatchingTableCell.identifier)
        tableView.register(NewLecturesTableCell.self, forCellReuseIdentifier: NewLecturesTableCell.identifier)
        tableView.register(LectureTableCell.self, forCellReuseIdentifier: LectureTableCell.identifier)
        tableView.register(PodcastTableCell.self, forCellReuseIdentifier: PodcastTableCell.identifier)


    }
    
    private func setupUI() {
        
        
        [highStack, tableView, lowScreenMenu].forEach { view.addSubview($0) }
        
        [companyLabel, searchView].forEach { highStack.addSubview($0) }
        
        highStack.layer.backgroundColor = UIColor(red: 0.059, green: 0.105, blue: 0.167, alpha: 1).cgColor
        
        tableView.delegate = self
        tableView.dataSource = self
        lowScreenMenu.layer.backgroundColor = UIColor(red: 0.145, green: 0.2, blue: 0.283, alpha: 1).cgColor
        
        lowScreenMenu.addSubview(lowStack)
        
        [browseButton, lecturesButton, coursesButton, profileButton].forEach { lowStack.addArrangedSubview($0) }
        lowStack.distribution = .fillEqually
        lowStack.axis = .horizontal
        lowStack.spacing = 5
        
        let logoImage = UIImage(named: "logo.png")
        companyLabel.image = logoImage
        
        let searchImage = UIImage(named: "search.png")
        searchView.image = searchImage
        
        
        let homeImage = UIImage(named: "home.png")
        browseButton.setImage(homeImage, for: .normal)
        
        let lectureImage = UIImage(named: "lecture.png")
        lecturesButton.setImage(lectureImage, for: .normal)
        
        let courseImage = UIImage(named: "course.png")
        coursesButton.setImage(courseImage, for: .normal)
        
        let profileImage = UIImage(named: "profile.png")
        profileButton.setImage(profileImage, for: .normal)
        
    }
    
    private func constraintUI() {
        
        highStack.anchor(top: view.safeAreaLayoutGuide.topAnchor,
                         leading: view.safeAreaLayoutGuide.leadingAnchor,
                         bottom: tableView.safeAreaLayoutGuide.topAnchor,
                         trailing: view.safeAreaLayoutGuide.trailingAnchor,
                         padding: .init(top: 0, left: 0, bottom: 0, right: 0))
        highStack.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        companyLabel.anchor(top: nil,
                            leading: highStack.safeAreaLayoutGuide.leadingAnchor,
                            bottom: nil, trailing: nil,
                            padding: .init(top: 0, left: 10, bottom: 0, right: 10))
        searchView.anchor(top: nil,
                          leading: nil, bottom: nil,
                          trailing: highStack.safeAreaLayoutGuide.trailingAnchor,
                          padding: .init(top: 0, left: 10, bottom: 0, right: 10))
        [searchView, companyLabel].forEach { $0.centerYAnchor.constraint(equalTo: highStack.centerYAnchor).isActive = true }
        
        
        tableView.anchor(top: highStack.safeAreaLayoutGuide.bottomAnchor,
                         leading: view.safeAreaLayoutGuide.leadingAnchor,
                         bottom: lowScreenMenu.safeAreaLayoutGuide.topAnchor,
                         trailing: view.safeAreaLayoutGuide.trailingAnchor,
                         padding: .init(top: 10, left: 10, bottom: 0, right: 10))
        
        
        lowScreenMenu.anchor(top: nil,
                             leading: view.safeAreaLayoutGuide.leadingAnchor,
                             bottom: view.safeAreaLayoutGuide.bottomAnchor,
                             trailing: view.safeAreaLayoutGuide.trailingAnchor)
        
        lowScreenMenu.heightAnchor.constraint(equalToConstant: 100).isActive = true
        lowScreenMenu.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        
        lowStack.anchor(top: lowScreenMenu.safeAreaLayoutGuide.topAnchor,
                        leading: lowScreenMenu.safeAreaLayoutGuide.leadingAnchor,
                        bottom: lowScreenMenu.safeAreaLayoutGuide.bottomAnchor,
                        trailing: lowScreenMenu.safeAreaLayoutGuide.trailingAnchor,
                        padding: .init(top: 20, left: 10, bottom: 30, right: 20))
        
    }
    
    
}


extension ViewController: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            return HeaderView(titleText: "Популярные лекции")
        } else if section == 1 {
            return HeaderView(titleText: "Продолжить просмотр")
        } else if section == 2 {
            return HeaderView(titleText: "Новые лекции")
        } else if section == 3 {
            return HeaderView(titleText: "Курсы")
        } else  {
            return HeaderView(titleText: "Новое в подкастах")
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 {
            return 340
        } else if indexPath.section == 1 {
            return 180
        } else if indexPath.section == 2 {
            return 270
        } else if indexPath.section == 3 {
            return 270
        } else {
            return 180
        }
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return CellTypes.allCases.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellTypes = sectionType[indexPath.section]

        switch cellTypes {
        case .popular:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: PopularLecturesTableCell.identifier, for: indexPath) as? PopularLecturesTableCell else { return UITableViewCell() }
            
            return cell
            
        case .continueWatching:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: ContinueWatchingTableCell.identifier, for: indexPath) as? ContinueWatchingTableCell else { return UITableViewCell() }
            
            return cell
            
        case .newLectures:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: NewLecturesTableCell.identifier, for: indexPath) as? NewLecturesTableCell else { return UITableViewCell() }
            
            return cell
            
            
        case .courses:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: LectureTableCell.identifier, for: indexPath) as? LectureTableCell else { return UITableViewCell() }
            
            return cell
            
        case .newPodcasts:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: PodcastTableCell.identifier, for: indexPath) as? PodcastTableCell else { return UITableViewCell() }
            
            return cell
            
        
        }
    }
}
