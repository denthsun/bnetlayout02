//
//  LectureCollectionCell.swift
//  bnetLayout02
//
//  Created by Denis Velikanov on 24.09.2021.
//

import UIKit

class LectureCollectionCell: UICollectionViewCell {
    
    static let identifier = "LectureCollectionCell"
    
    let cellImage = UIImageView()
    let lectureCounterImage = UIImageView()
    
    let lowStack = UIView()
    let titleLabel = UILabel()
    let typeLabel = UILabel()
    
    let watcherStack = UIView()
    let watchImage = UIImageView()
    let watchersLabel = UILabel()
    let gradientView = GradientView(colors: [UIColor.clear.cgColor, UIColor.black.cgColor])

    
    override class func awakeFromNib() {
        
    }
    
    private func setupUI() {
        [cellImage, lowStack].forEach { contentView.addSubview($0) }
        [gradientView].forEach { cellImage.addSubview($0) }
        [lectureCounterImage].forEach { gradientView.addSubview($0) }
        [titleLabel, typeLabel, watcherStack].forEach { lowStack.addSubview($0) }
        [watchImage, watchersLabel].forEach { watcherStack.addSubview($0) }
        let headImage = UIImage(named: "lectures.png")
        lectureCounterImage.image = headImage
        
        
        let lectureImage = UIImage(named: "lectureCell.png")
        cellImage.image = lectureImage
        cellImage.enableCornerRadius(radius: 10)
        
        let eyeImage = UIImage(named: "visible.png")
        watchImage.image = eyeImage
        
        watchersLabel.textColor = .lightGray
        watchersLabel.text = "45"
        
        titleLabel.text = "Роберт Сапольски. Биология поведения человека"
        titleLabel.numberOfLines = 0
        titleLabel.textColor = .white
        titleLabel.font = UIFont.systemFont(ofSize: 14)
        titleLabel.adjustsFontSizeToFitWidth = true
        
        typeLabel.text = "Нейрофизиология"
        typeLabel.textColor = .red
        typeLabel.font = UIFont.systemFont(ofSize: 14)
        
        
        
    }
    
    private func constraintUI() {
        cellImage.anchor(top: contentView.safeAreaLayoutGuide.topAnchor,
                         leading: contentView.safeAreaLayoutGuide.leadingAnchor,
                         bottom: lowStack.safeAreaLayoutGuide.topAnchor,
                         trailing: contentView.safeAreaLayoutGuide.trailingAnchor)
        gradientView.anchor(top: cellImage.safeAreaLayoutGuide.topAnchor,
                            leading: cellImage.safeAreaLayoutGuide.leadingAnchor,
                            bottom: cellImage.safeAreaLayoutGuide.bottomAnchor,
                            trailing: cellImage.safeAreaLayoutGuide.trailingAnchor,
                            padding: .init(top: 0, left: 0, bottom: -10, right: 0))
        lectureCounterImage.anchor(top: nil,
                                leading: gradientView.safeAreaLayoutGuide.leadingAnchor,
                                bottom: gradientView.safeAreaLayoutGuide.bottomAnchor,
                                trailing: nil,
                                padding: .init(top: 0, left: 10, bottom: 10, right: 0))

        lowStack.anchor(top: cellImage.safeAreaLayoutGuide.bottomAnchor,
                        leading: contentView.safeAreaLayoutGuide.leadingAnchor,
                        bottom: contentView.safeAreaLayoutGuide.bottomAnchor,
                        trailing: contentView.safeAreaLayoutGuide.trailingAnchor,
                        padding: .init(top: 10, left: 0, bottom: 10, right: 10))
        lowStack.heightAnchor.constraint(equalToConstant: 60).isActive = true
        
        titleLabel.anchor(top: lowStack.safeAreaLayoutGuide.topAnchor,
                          leading: lowStack.safeAreaLayoutGuide.leadingAnchor,
                          bottom: typeLabel.safeAreaLayoutGuide.topAnchor,
                          trailing: lowStack.safeAreaLayoutGuide.trailingAnchor)
        typeLabel.anchor(top: titleLabel.safeAreaLayoutGuide.bottomAnchor,
                         leading: lowStack.safeAreaLayoutGuide.leadingAnchor,
                         bottom: lowStack.safeAreaLayoutGuide.bottomAnchor,
                         trailing: nil)
        
        watcherStack.anchor(top: nil,
                            leading: nil,
                             bottom: lowStack.safeAreaLayoutGuide.bottomAnchor,
                             trailing: lowStack.safeAreaLayoutGuide.trailingAnchor)
        watcherStack.centerYAnchor.constraint(equalTo: typeLabel.centerYAnchor).isActive = true
        
        watchImage.anchor(top: nil,
                          leading: watcherStack.safeAreaLayoutGuide.leadingAnchor,
                          bottom: nil,
                          trailing: watchersLabel.safeAreaLayoutGuide.leadingAnchor)
        watchImage.heightAnchor.constraint(equalToConstant: 16).isActive = true
        watchImage.widthAnchor.constraint(equalToConstant: 16).isActive = true
        watchImage.centerYAnchor.constraint(equalTo: watchersLabel.centerYAnchor).isActive = true

        watchersLabel.anchor(top: watcherStack.safeAreaLayoutGuide.topAnchor,
                             leading: watchImage.safeAreaLayoutGuide.trailingAnchor,
                             bottom: watcherStack.safeAreaLayoutGuide.bottomAnchor,
                             trailing: watcherStack.safeAreaLayoutGuide.trailingAnchor)
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        setupUI()
        constraintUI()
        backgroundColor =  UIColor(red: 0.082, green: 0.129, blue: 0.192, alpha: 1)
        self.enableCornerRadius(radius: 10)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
