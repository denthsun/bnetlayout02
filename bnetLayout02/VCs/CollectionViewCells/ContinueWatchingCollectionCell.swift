//
//  ContinueWatchingCollectionCell.swift
//  bnetLayout02
//
//  Created by Denis Velikanov on 23.09.2021.
//

import UIKit

class ContinueWatchingCollectionCell: UICollectionViewCell {
    
    static let identifier = "ContinueWatchingCollectionCell"
    
    
    let leftCellStack = UIView()
    let cellImage = UIImageView()
    let durationLabel = UILabel()
    let durationView = UIView()
    let gradientView = GradientView(colors: [UIColor.clear.cgColor, UIColor.black.cgColor])
    
    let rightCellStack = UIView()
    let titleLabel = UILabel()
    let typeLabel = UILabel()
    
    private func setupUI() {
        [leftCellStack, rightCellStack].forEach { contentView.addSubview($0) }
        
        [cellImage].forEach { leftCellStack.addSubview($0) }
        cellImage.addSubview(gradientView)
        [durationLabel, durationView].forEach { gradientView.addSubview($0) }
        [titleLabel, typeLabel].forEach { rightCellStack.addSubview($0) }
        
        cellImage.image = UIImage(named: "moda")
        cellImage.contentMode = .scaleAspectFill
        cellImage.enableCornerRadius(radius: 10)
        
        
        durationLabel.text = "25 из 45 мин"
        durationLabel.textColor = .white
        
        durationView.backgroundColor = .red
        
        titleLabel.text = "Во что одеты люди на известных портретах"
        titleLabel.textColor = .white
        titleLabel.numberOfLines = 0
        
        typeLabel.text = "Живопись"
        typeLabel.textColor = .red
    }
    
    private func constraintUI() {
        leftCellStack.anchor(top: contentView.safeAreaLayoutGuide.topAnchor,
                             leading: contentView.safeAreaLayoutGuide.leadingAnchor,
                             bottom: contentView.safeAreaLayoutGuide.bottomAnchor,
                             trailing: rightCellStack.safeAreaLayoutGuide.leadingAnchor)
        cellImage.anchor(top: leftCellStack.safeAreaLayoutGuide.topAnchor,
                         leading: leftCellStack.safeAreaLayoutGuide.leadingAnchor,
                         bottom: leftCellStack.safeAreaLayoutGuide.bottomAnchor,
                         trailing: leftCellStack.safeAreaLayoutGuide.trailingAnchor)
        cellImage.widthAnchor.constraint(equalTo: leftCellStack.widthAnchor).isActive = true
        
        gradientView.anchor(top: cellImage.safeAreaLayoutGuide.topAnchor,
                            leading: cellImage.safeAreaLayoutGuide.leadingAnchor,
                            bottom: cellImage.safeAreaLayoutGuide.bottomAnchor,
                            trailing: cellImage.safeAreaLayoutGuide.trailingAnchor)
        
        durationLabel.anchor(top: nil,
                             leading: gradientView.safeAreaLayoutGuide.leadingAnchor,
                             bottom: durationView.safeAreaLayoutGuide.topAnchor,
                             trailing: gradientView.safeAreaLayoutGuide.trailingAnchor,
                             padding: .init(top: 0, left: 10, bottom: 15, right: 0))
        durationLabel.heightAnchor.constraint(equalToConstant: 25).isActive = true
 
        durationView.anchor(top: durationLabel.safeAreaLayoutGuide.bottomAnchor,
                            leading: gradientView.safeAreaLayoutGuide.leadingAnchor,
                            bottom: gradientView.safeAreaLayoutGuide.bottomAnchor,
                            trailing: gradientView.safeAreaLayoutGuide.trailingAnchor,
                            padding: .init(top: 0, left: 0, bottom: 0, right: 40))
        durationView.heightAnchor.constraint(equalToConstant: 5).isActive = true
        durationView.widthAnchor.constraint(equalToConstant: leftCellStack.layer.bounds.width - 30).isActive = true
        
        rightCellStack.anchor(top: contentView.safeAreaLayoutGuide.topAnchor,
                              leading: leftCellStack.safeAreaLayoutGuide.trailingAnchor,
                              bottom: contentView.safeAreaLayoutGuide.bottomAnchor,
                              trailing: contentView.safeAreaLayoutGuide.trailingAnchor)
        titleLabel.anchor(top: rightCellStack.safeAreaLayoutGuide.topAnchor,
                          leading: rightCellStack.safeAreaLayoutGuide.leadingAnchor,
                          bottom: nil,
                          trailing: rightCellStack.safeAreaLayoutGuide.trailingAnchor,
                          padding: .init(top: 0, left: 10, bottom: 0, right: 10))
        typeLabel.anchor(top: nil,
                         leading: rightCellStack.safeAreaLayoutGuide.leadingAnchor,
                         bottom: rightCellStack.safeAreaLayoutGuide.bottomAnchor,
                         trailing: rightCellStack.safeAreaLayoutGuide.trailingAnchor,
                         padding: .init(top: 0, left: 10, bottom: 0, right: 10))
        leftCellStack.widthAnchor.constraint(equalTo: rightCellStack.widthAnchor).isActive = true
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        setupUI()
        constraintUI()
        backgroundColor =  UIColor(red: 0.082, green: 0.129, blue: 0.192, alpha: 1)
        self.enableCornerRadius(radius: 10)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
