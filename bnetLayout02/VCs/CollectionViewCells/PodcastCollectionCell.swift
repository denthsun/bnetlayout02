//
//  PodcastCollectionCell.swift
//  bnetLayout02
//
//  Created by Denis Velikanov on 24.09.2021.
//

import UIKit

class PodcastCollectionCell: UICollectionViewCell {
    
    static let identifier = "PodcastCollectionCell"
    
    
    let leftCellStack = UIView()
    let cellImage = UIImageView()
    let headPhonesImage = UIImageView()
    
    let rightCellStack = UIView()
    let titleLabel = UILabel()
    let typeLabel = UILabel()
    let timeImage = UIImageView()
    
    private func setupUI() {
        contentView.layer.backgroundColor = UIColor(red: 0.145, green: 0.2, blue: 0.283, alpha: 1).cgColor

        
        [leftCellStack, rightCellStack].forEach { contentView.addSubview($0) }
        
        [cellImage, headPhonesImage].forEach { leftCellStack.addSubview($0) }
        [titleLabel, typeLabel, timeImage].forEach { rightCellStack.addSubview($0) }
        
        cellImage.image = UIImage(named: "cellImage02")
        cellImage.contentMode = .scaleToFill
        
 
        headPhonesImage.image = UIImage(named: "headphones.png")
        
        titleLabel.text = "Во что одеты люди на известных портретах"
        titleLabel.textColor = .white
        titleLabel.numberOfLines = 0
        
        typeLabel.text = "Живопись"
        typeLabel.textColor = .red
        
        timeImage.image = UIImage(named: "time.png")
    }
    
    private func constraintUI() {
        leftCellStack.anchor(top: contentView.safeAreaLayoutGuide.topAnchor,
                             leading: contentView.safeAreaLayoutGuide.leadingAnchor,
                             bottom: contentView.safeAreaLayoutGuide.bottomAnchor,
                             trailing: rightCellStack.safeAreaLayoutGuide.leadingAnchor)
        leftCellStack.widthAnchor.constraint(equalToConstant: 100).isActive = true
        cellImage.anchor(top: leftCellStack.safeAreaLayoutGuide.topAnchor,
                         leading: leftCellStack.safeAreaLayoutGuide.leadingAnchor,
                         bottom: leftCellStack.safeAreaLayoutGuide.bottomAnchor,
                         trailing: leftCellStack.safeAreaLayoutGuide.trailingAnchor)
        headPhonesImage.anchor(top: nil,
                               leading: cellImage.safeAreaLayoutGuide.leadingAnchor,
                               bottom: cellImage.safeAreaLayoutGuide.bottomAnchor,
                               trailing: nil,
                               padding: .init(top: 0, left: 5, bottom: 5, right: 0))
        
        rightCellStack.anchor(top: contentView.safeAreaLayoutGuide.topAnchor,
                              leading: leftCellStack.safeAreaLayoutGuide.trailingAnchor,
                              bottom: contentView.safeAreaLayoutGuide.bottomAnchor,
                              trailing: contentView.safeAreaLayoutGuide.trailingAnchor,
                              padding: .init(top: 10, left: 10, bottom: 10, right: 10))
        titleLabel.anchor(top: rightCellStack.safeAreaLayoutGuide.topAnchor,
                          leading: rightCellStack.safeAreaLayoutGuide.leadingAnchor,
                          bottom: nil,
                          trailing: rightCellStack.safeAreaLayoutGuide.trailingAnchor,
                          padding: .init(top: 0, left: 10, bottom: 0, right: 10))
        typeLabel.anchor(top: nil,
                         leading: rightCellStack.safeAreaLayoutGuide.leadingAnchor,
                         bottom: rightCellStack.safeAreaLayoutGuide.bottomAnchor,
                         trailing: rightCellStack.safeAreaLayoutGuide.trailingAnchor,
                         padding: .init(top: 0, left: 10, bottom: 0, right: 10))
        
        timeImage.anchor(top: nil,
                         leading: nil,
                         bottom: rightCellStack.safeAreaLayoutGuide.bottomAnchor,
                         trailing: rightCellStack.safeAreaLayoutGuide.trailingAnchor,
                         padding: .init(top: 0, left: 0, bottom: 10, right: 10))
        
        timeImage.centerYAnchor.constraint(equalTo: typeLabel.centerYAnchor).isActive = true
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        setupUI()
        constraintUI()
        backgroundColor =  UIColor(red: 0.082, green: 0.129, blue: 0.192, alpha: 1)
        self.enableCornerRadius(radius: 10)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
