//
//  ContinueWatchingTableCell.swift
//  bnetLayout02
//
//  Created by Denis Velikanov on 23.09.2021.
//

import UIKit

class ContinueWatchingTableCell: UITableViewCell {
    
    static let identifier = "ContinueWatchingTableCell"
    
    let collectionView = UICollectionView(frame: .init(),
                                          collectionViewLayout: UICollectionViewFlowLayout.init())
    let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout.init()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func setLayout() {
        layout.scrollDirection = .horizontal
        layout.itemSize = .init(width: 350, height: 130)
        collectionView.setCollectionViewLayout(layout, animated: true)
    }
    
    private func setupUI() {
        contentView.addSubview(collectionView)
        collectionView.register(ContinueWatchingCollectionCell.self, forCellWithReuseIdentifier: ContinueWatchingCollectionCell.identifier)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.layer.backgroundColor = UIColor(red: 0.082, green: 0.129, blue: 0.192, alpha: 1).cgColor
    }
    
    private func constraintUI() {
        collectionView.anchor(top: contentView.safeAreaLayoutGuide.topAnchor,
                              leading: contentView.safeAreaLayoutGuide.leadingAnchor,
                              bottom: contentView.safeAreaLayoutGuide.bottomAnchor,
                              trailing: contentView.safeAreaLayoutGuide.trailingAnchor)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
        constraintUI()
        setLayout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension ContinueWatchingTableCell: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ContinueWatchingCollectionCell.identifier, for: indexPath) as? ContinueWatchingCollectionCell else { return UICollectionViewCell() }
        
        
        
        return cell
    }
    
}
